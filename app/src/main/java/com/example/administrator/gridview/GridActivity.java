package com.example.administrator.gridview;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.example.administrator.gridview.R;

import java.util.ArrayList;

public class GridActivity extends Activity implements AdapterView.OnItemClickListener, View.OnClickListener {

    //行
    int ROWS = 4;
    //列
    int COLUMNS = 6;

    GridView gridView;
    ArrayList<Status> gridList = new ArrayList<>();
    GridAdapter adapter;

    GridView hListView;
    ListView vListView;
    ArrayList<Status> hList = new ArrayList<>();
    ArrayList<Status> vList = new ArrayList<>();
    ListViewAdapter vAdapter, hAdapter;

    RelativeLayout btn_all;
    boolean isAllSelect = false;
    ImageView img_all;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid);
        findViewById();

        GridView();
        VListView();
        HListView();
    }

    public void findViewById() {
        gridView = (GridView) findViewById(R.id.gridview);
        vListView = (ListView) findViewById(R.id.vListView);
        hListView = (GridView) findViewById(R.id.hListView);

        btn_all = (RelativeLayout) findViewById(R.id.btn_all);
        btn_all.setOnClickListener(this);
        img_all = (ImageView) findViewById(R.id.img_all);
    }

    public void GridView() {

        adapter = new GridAdapter(this, gridList);
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(this);

        for (int i = 0; i < ROWS * COLUMNS; i++) {
            Status gridStatus = new Status();
            gridStatus.setPosition(i);
            gridStatus.setSelect(false);
            gridList.add(gridStatus);
        }
        adapter.notifyDataSetChanged();
    }

    public void VListView() {

        vAdapter = new ListViewAdapter(this, vList);
        vListView.setAdapter(vAdapter);
        vListView.setOnItemClickListener(vOnItemClick);

        for (int i = 0; i < ROWS; i++) {
            Status vStatus = new Status();
            vStatus.setPosition(i);
            vStatus.setSelect(false);
            vList.add(vStatus);
        }
        vAdapter.notifyDataSetChanged();
    }

    public void HListView() {

        hAdapter = new ListViewAdapter(this, hList);
        hListView.setAdapter(hAdapter);
        hListView.setOnItemClickListener(hOnItemClick);

        for (int i = 0; i < COLUMNS; i++) {
            Status vStatus = new Status();
            vStatus.setPosition(i);
            vStatus.setSelect(false);
            hList.add(vStatus);
        }
        hAdapter.notifyDataSetChanged();
    }

    /**
     * 行
     */
    AdapterView.OnItemClickListener vOnItemClick = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
            int start = position * COLUMNS;
            int end = (position + 1) * COLUMNS;
            if (vList.get(position).getSelect() == false) {
                for (int i = start; i < end; i++) {
                    gridList.get(i).setSelect(true);
                }
                vList.get(position).setSelect(true);
            } else {
                for (int i = start; i < end; i++) {
                    gridList.get(i).setSelect(false);
                }
                vList.get(position).setSelect(false);
            }
            vAdapter.notifyDataSetChanged();
            adapter.notifyDataSetChanged();
            //判断列及全选按钮状态
            isColumns();
            isAllSelect();
        }
    };

    /**
     * 列
     */
    AdapterView.OnItemClickListener hOnItemClick = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
            if (hList.get(position).getSelect() == false) {
                for (int i = 0; i < gridList.size() / COLUMNS; i++) {
                    int pos = i * COLUMNS + position;
                    gridList.get(pos).setSelect(true);
                }
                hList.get(position).setSelect(true);
            } else {
                for (int i = 0; i < gridList.size() / COLUMNS; i++) {
                    int pos = i * COLUMNS + position;
                    gridList.get(pos).setSelect(false);
                }
                hList.get(position).setSelect(false);
            }
            hAdapter.notifyDataSetChanged();
            adapter.notifyDataSetChanged();
            //判断行及全选按钮状态
            isRows();
            isAllSelect();
        }
    };

    /**
     * 单元格
     */
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        if (gridList.get(position).getSelect() == false) {
            gridList.get(position).setSelect(true);
        } else if (gridList.get(position).getSelect() == true) {
            gridList.get(position).setSelect(false);
        }
        adapter.notifyDataSetChanged();

        isColumns();
        isRows();
        isAllSelect();
    }

    /**
     * 全选
     */
    @Override
    public void onClick(View view) {
        if (isAllSelect == false) {
            for (int i = 0; i < gridList.size(); i++) {
                gridList.get(i).setSelect(true);
            }
            for (int i = 0; i < vList.size(); i++) {
                vList.get(i).setSelect(true);
            }
            for (int i = 0; i < hList.size(); i++) {
                hList.get(i).setSelect(true);
            }
            isAllSelect = true;
            img_all.setImageResource(R.drawable.shape_orange);
        } else if (isAllSelect == true) {
            for (int i = 0; i < gridList.size(); i++) {
                gridList.get(i).setSelect(false);
            }
            for (int i = 0; i < vList.size(); i++) {
                vList.get(i).setSelect(false);
            }
            for (int i = 0; i < hList.size(); i++) {
                hList.get(i).setSelect(false);
            }
            isAllSelect = false;
            img_all.setImageResource(R.drawable.shape_green);
        }
        vAdapter.notifyDataSetChanged();
        hAdapter.notifyDataSetChanged();
        adapter.notifyDataSetChanged();
    }

    /**
     * 判断全选
     */
    public void isAllSelect(){
        int j = 0;
        for (int i = 0;i<gridList.size();i++){
            if (gridList.get(i).getSelect() == true){
                j++;
            }
        }
        if (j==gridList.size()){
            isAllSelect = true;
            img_all.setImageResource(R.drawable.shape_orange);
        }else{
            isAllSelect = false;
            img_all.setImageResource(R.drawable.shape_green);
        }
    }

    /**
     * 判断列
     */
    public void isColumns(){
        for (int i = 0;i<hList.size();i++){
            int j = 0;
            for (int k = 0;k<vList.size();k++){
                int pos = k * COLUMNS + i;
                if (gridList.get(pos).getSelect() == true){
                    j++;
                }
            }
            if (j==ROWS){
                hList.get(i).setSelect(true);
            }else {
                hList.get(i).setSelect(false);
            }
            hAdapter.notifyDataSetChanged();
        }
    }

    /**
     * 判断行
     */
    public void isRows(){
        for (int i = 0; i < vList.size(); i++) {
            int j = 0;
            int start = i * COLUMNS;
            int end = (i + 1) * COLUMNS;
            for (int k = start; k < end; k++) {
                if (gridList.get(k).getSelect() == true) {
                    j++;
                }
            }
            if (j == COLUMNS) {
                vList.get(i).setSelect(true);
            } else {
                vList.get(i).setSelect(false);
            }
            vAdapter.notifyDataSetChanged();
        }
    }
}
