package com.example.administrator.gridview;

/**
 * Created by Administrator on 2016/2/19.
 */
public class Status {
    Integer type = 0;
    Integer position = 0;
    Boolean isSelect = false;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Boolean getSelect() {
        return isSelect;
    }

    public void setSelect(Boolean select) {
        isSelect = select;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }
}
