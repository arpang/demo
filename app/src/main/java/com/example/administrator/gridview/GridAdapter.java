package com.example.administrator.gridview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.example.administrator.gridview.R;

import java.util.ArrayList;

/**
 * Created by Administrator on 2016/2/19.
 */
public class GridAdapter extends BaseAdapter {

    Context context;
    ArrayList<Status> list;

    public GridAdapter(Context context, ArrayList<Status> list) {
        this.context = context;
        this.list = list;
    }


    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = mInflater.inflate(R.layout.item_shape, null);
        Status gridStatus = list.get(position);
        ImageView image = (ImageView) convertView.findViewById(R.id.image);
        if (gridStatus.getSelect() == false) {
            image.setImageResource(R.drawable.shape_blue);
        } else {
            image.setImageResource(R.drawable.shape_red);
        }
        return convertView;
    }
}
