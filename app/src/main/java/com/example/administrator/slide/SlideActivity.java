package com.example.administrator.slide;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import com.example.administrator.gridview.R;

public class SlideActivity extends FragmentActivity implements View.OnClickListener {

    FragmentManager fragmentManager;
    FragmentOne fg1;
    FragmentTwo fg2;
    Button btn_start;
    boolean isFg1 = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slide);
        //设置全屏
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        btn_start = (Button) findViewById(R.id.btn_start);
        btn_start.setOnClickListener(this);


        fragmentManager = getSupportFragmentManager();

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        fg1 = new FragmentOne();
        transaction.add(R.id.layout_frg, fg1);
        transaction.commit();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_start:
                init();
                break;
        }
    }

    public void init(){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (isFg1 == true) {
            transaction.setCustomAnimations(R.anim.slide_in_from_bottom,
                    R.anim.slide_out_to_bottom);
            if (fg2 == null){
                fg2 = new FragmentTwo();
                transaction.add(R.id.layout_frg, fg2);
            }else{
                transaction.show(fg2);
            }
            btn_start.setBackgroundColor(Color.WHITE);
            btn_start.setText("Start Incubation");
            isFg1 = false;
            if (fg1 != null){
                transaction.hide(fg1);
            }
        } else {
            if (fg2 != null){
                transaction.hide(fg2);
            }

            if (fg1 == null){
                fg1 = new FragmentOne();
                transaction.add(R.id.layout_frg, fg1);
            }else{
                transaction.show(fg1);
            }

            btn_start.setBackgroundColor(Color.GRAY);
            btn_start.setText("Place Cell Dish");
            isFg1 = true;
        }
        transaction.commit();
    }

}
