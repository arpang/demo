package com.example.administrator.slide;

import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ViewFlipper;

import com.example.administrator.gridview.R;

import java.util.ArrayList;
import java.util.List;

public class FragmentTwo extends Fragment implements View.OnClickListener{

    View View;
    ViewFlipper viewFlipper;
    Button btn_next,btn_before;

    //滚动条动画
    ImageView cursor;
    List<ImageView> imgList = new ArrayList<>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View = inflater.inflate(R.layout.fragment_two, container, false);
        //滚动条
        cursor = (ImageView) View.findViewById(R.id.img_slide);
        btn_next = (Button) View.findViewById(R.id.btn_next);
        btn_next.setOnClickListener(this);
        btn_before = (Button) View.findViewById(R.id.btn_before);
        btn_before.setOnClickListener(this);

        viewFlipper = (ViewFlipper) View.findViewById(R.id.vf1);
        viewFlipper.removeAllViews();
        View vf1 = LayoutInflater.from(getActivity()).inflate(R.layout.vf1,null);
        viewFlipper.addView(vf1);



        return View;
    }

    int Page = 0;
    @Override
    public void onClick(View view) {
        Animation animation = null;
        switch (view.getId()){
            case R.id.btn_before:     //上一页
                before();
                if (Page > 0){
                    animation = new TranslateAnimation(Page * 400,(Page - 1) * 400 ,0,0);
                    Page--;
                }
                break;
            case R.id.btn_next:       //下一页
                next();
                if (Page < 3){
                    animation = new TranslateAnimation(Page * 400,(Page + 1)*400,0,0);
                    Page++;
                }
                break;
        }
        if (animation!=null){
            animation.setFillAfter(true);
            animation.setDuration(500);
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    cursor.setImageResource(R.drawable.slide_on_bg1);
                    cursor.setPadding(0,0,0,4);
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    cursor.setImageResource(R.drawable.slide_on_bg);
                    cursor.setPadding(0,0,0,0);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            cursor.startAnimation(animation);
        }

    }


    public void next(){
        View vf2 = LayoutInflater.from(getActivity()).inflate(R.layout.vf2,null);
        viewFlipper.addView(vf2, 1);
        this.viewFlipper.setInAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.push_left_in));
        this.viewFlipper.setOutAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.push_left_out));
        this.viewFlipper.showNext();
        viewFlipper.removeViewAt(0);
    }

    public void before(){
        View vf1 = LayoutInflater.from(getActivity()).inflate(R.layout.vf1,null);
        viewFlipper.addView(vf1, -1);
        this.viewFlipper.setInAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.push_right_in));
        this.viewFlipper.setOutAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.push_right_out));
        this.viewFlipper.showNext();
        viewFlipper.removeViewAt(0);
    }
}
