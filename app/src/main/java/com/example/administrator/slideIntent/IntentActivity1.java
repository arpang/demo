package com.example.administrator.slideIntent;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.example.administrator.gridview.R;

public class IntentActivity1 extends Activity implements View.OnClickListener{

    Button btn;
    RelativeLayout relativeLayout1,relativeLayout2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intent1);

        findViewById();

    }

    public void findViewById(){
        btn = (Button) findViewById(R.id.btn);
        btn.setOnClickListener(this);

        relativeLayout1 = (RelativeLayout) findViewById(R.id.relativeLayout1);
        relativeLayout2 = (RelativeLayout) findViewById(R.id.relativeLayout2);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn:
                intent();
                break;
        }
    }

    int y;
    public void intent(){
        WindowManager wm = this.getWindowManager();
        int height = wm.getDefaultDisplay().getHeight();

        y = dip2px(this,80);
        TranslateAnimation animation = new TranslateAnimation(0,0, 0, y);
        animation.setDuration(300);
        animation.setFillAfter(false);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                relativeLayout1.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Intent intent = new Intent(IntentActivity1.this, IntentActivity2.class);
                startActivityForResult(intent, 100);
                overridePendingTransition(R.anim.intent_animation2, R.anim.intent_animation1);

                relativeLayout1.setVisibility(View.GONE);
                relativeLayout2.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        relativeLayout2.startAnimation(animation);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 100){
            relativeLayout1.setVisibility(View.VISIBLE);
            relativeLayout2.setVisibility(View.VISIBLE);
        }
    }

    public static int dip2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }
}
