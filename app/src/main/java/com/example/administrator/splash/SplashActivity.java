package com.example.administrator.splash;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.administrator.Utils;
import com.example.administrator.gridview.R;

public class SplashActivity extends Activity {

    ImageView img_back,img_pic,img_pic1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        findViewById();

        animation();
        animation1();
    }

    public void findViewById(){
        img_back = (ImageView) findViewById(R.id.img_back);
        Glide.with(this).load(R.drawable.splash_bg).into(img_back);

        img_pic = (ImageView) findViewById(R.id.img_pic);

        img_pic1 = (ImageView) findViewById(R.id.img_pic1);
    }

    public void animation(){
        WindowManager wm = this.getWindowManager();
        int width = wm.getDefaultDisplay().getWidth();

        TranslateAnimation animation = new TranslateAnimation( 0, width,0,0);
        animation.setDuration(3000);
        animation.setRepeatCount(100);
        animation.setFillAfter(false);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        img_pic.startAnimation(animation);
    }

    public void animation1(){
        WindowManager wm = this.getWindowManager();
        int width = wm.getDefaultDisplay().getWidth();

        TranslateAnimation animation = new TranslateAnimation(-width ,0 ,0,0);
        animation.setDuration(3000);
        animation.setRepeatCount(100);
        animation.setFillAfter(false);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        img_pic1.startAnimation(animation);
    }
}
