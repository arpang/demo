package com.example.administrator;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.administrator.button.ButtonActivity;
import com.example.administrator.gridview.GridActivity;
import com.example.administrator.gridview.R;
import com.example.administrator.slide.SlideActivity;
import com.example.administrator.slideIntent.IntentActivity1;
import com.example.administrator.splash.SplashActivity;

public class MainActivity extends Activity implements View.OnClickListener{

    Button btn_grid,btn_slide,btn_slide1,btn_btn,btn_splash;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById();
    }

    public void findViewById(){
        btn_grid = (Button) findViewById(R.id.btn_grid);
        btn_grid.setOnClickListener(this);

        btn_slide = (Button) findViewById(R.id.btn_slide);
        btn_slide.setOnClickListener(this);

        btn_slide1 = (Button) findViewById(R.id.btn_slide1);
        btn_slide1.setOnClickListener(this);

        btn_btn = (Button) findViewById(R.id.btn_btn);
        btn_btn.setOnClickListener(this);

        btn_splash = (Button) findViewById(R.id.btn_splash);
        btn_splash.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_grid:
                Intent Grid = new Intent(this, GridActivity.class);
                startActivity(Grid);
                break;
            case R.id.btn_slide:
                Intent Slide = new Intent(this, SlideActivity.class);
                startActivity(Slide);
                break;
            case R.id.btn_slide1:
                Intent intent = new Intent(this, IntentActivity1.class);
                startActivity(intent);
                break;
            case R.id.btn_btn:
                Intent btn = new Intent(this, ButtonActivity.class);
                startActivity(btn);
                break;
            case R.id.btn_splash:
                Intent splash = new Intent(this, SplashActivity.class);
                startActivity(splash);
                break;
        }
    }
}
