
package com.example.administrator.button;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;

import com.example.administrator.Utils;
import com.example.administrator.gridview.R;

public class ButtonActivity extends Activity implements View.OnClickListener {

    Button btn;
    ImageView img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_button);

        findViewById();

    }

    public void findViewById(){
        btn = (Button) findViewById(R.id.btn);
        btn.setOnClickListener(this);

        img = (ImageView) findViewById(R.id.img);
        img.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn:
                if (isButton == false){
                    button();
                    isButton = true;
                    btn.setText("结束");
                }else{
                    img.clearAnimation();
                    isButton = false;
                    btn.setText("开始");
                    img.setVisibility(View.GONE);
                }
                break;
        }
    }

    boolean isButton = false;
    int y;
    public void button(){
        y = Utils.dip2px(this,100);
        TranslateAnimation animation = new TranslateAnimation(0,0, -y, y);
        animation.setDuration(1000);
        animation.setRepeatCount(100);
        animation.setFillAfter(true);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                img.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        img.startAnimation(animation);
    }
}
